//
//  invitadoViewController.swift
//  AppClimaGR1
//
//  Created by Jaime Yerovi on 25/10/17.
//  Copyright © 2017 jy. All rights reserved.
//

import UIKit
import CoreLocation

class invitadoViewController: UIViewController, CLLocationManagerDelegate {

    //MARK:- Outles
    
    
    @IBOutlet weak var weatherLabel: UILabel!
    
    let locationManager = CLLocationManager()
  //  var bandera = false
    
    //MARK:- ViewController Lyfecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        }
    }

    //MARK:- LocationManager Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate
       // if !bandera {
            consultarPorUbicacion(lat: (location?.latitude)!, lon: (location?.longitude)!)
//            bandera = true
//       }
        locationManager.stopUpdatingLocation()
        
    }
    
    //MARK:- Actions
    
    private func consultarPorUbicacion(lat:Double, lon:Double) {
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=be5f29ecd9f9aa1d4f0cb1a555a07012"
        
        let url = URL(string: urlStr)
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request) { (data, response, err) in
            if let _ = err {
                return
            }
            
            do {
                let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
                let weatherDict = weatherJson as! NSDictionary
                guard let weatherKey = weatherDict["weather"] as? NSArray else {
                    DispatchQueue.main.async {
                        self.weatherLabel.text = "Ciudad no valida"
                    }
                    return
                }
                let weather = weatherKey[0] as! NSDictionary
                DispatchQueue.main.async {
                    self.weatherLabel.text = "\(weather["description"] ?? "Error")"
                }
            } catch {
                print("Error al generar el Json")
            }
        }
        
        task.resume()
    }

    
    

}
