//
//  CiudadViewController.swift
//  AppClimaGR1
//
//  Created by Jaime Yerovi on 31/10/17.
//  Copyright © 2017 jy. All rights reserved.
//

//apikey 9f3fbd7a3db184103a6aaeba5ea37a2a

//http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=9f3fbd7a3db184103a6aaeba5ea37a2a



import UIKit

class CiudadViewController: UIViewController {

    //MARK:- Outlets
    
    
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var weatherLabel: UILabel!
    
    
    //MARK:- ViewController lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    //MARK:- Actions
    
    
    @IBAction func consultarButtonPressed(_ sender: Any) {
        
        let urlStr = "http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=9f3fbd7a3db184103a6aaeba5ea37a2a"
        
        let url = URL(string: urlStr)
        let request = URLRequest(url: url!)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            print("Error: \(error)")
            if let _ = error {
                return
            }
            do {
                
                let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
                
                let weatherDict = weatherJson as! NSDictionary
                
                guard let weatherKey = weatherDict["weather"] as? NSArray else{
                    DispatchQueue.main.async {
                        self.weatherLabel.text = "Ciudad no valida"
                    }
                    return
                }
                let weather = weatherKey[0] as! NSDictionary
                
                DispatchQueue.main.async {
                    self.weatherLabel.text = "\(weather["description"] ?? "Error")"
                }
                
                
                print(weatherJson)
                
            } catch {
                print("Error al generar Json")
            }
        }
        
        task.resume()
        
        
    }
    
  
}
