//
//  ViewController.swift
//  AppClimaGR1
//
//  Created by Jaime Yerovi on 25/10/17.
//  Copyright © 2017 jy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var userNameTextField: UITextField!
    
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    //MARK:- ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        userNameTextField.text = ""
        passwordTextField.text = ""
        userNameTextField.becomeFirstResponder() //foco del puntero
    }
    
    
    //MARK:- Actions
    
    @IBAction func entrarButtonPressed(_ sender: Any) {
        
        let user = userNameTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        switch(user,password){
            
        case("jaime", "jaime"):
            performSegue(withIdentifier: "ciudadSegue", sender: self)
        case("jaime", _):
            mostrarAlerta(mensaje: "Contraseña incorrecta")
        default:
            mostrarAlerta(mensaje: "usuario y contraseña incorrectas")
            
        }
        
    }
    
    private func mostrarAlerta(mensaje:String){
        let alertView = UIAlertController(title: "Error", message: mensaje, preferredStyle: .alert)
        
        let aceptar = UIAlertAction(title: "ACEPTAR", style: .default) { (action) in
            self.userNameTextField.text = ""
            self.passwordTextField.text = ""
        }
        
        alertView.addAction(aceptar)
        
        present(alertView, animated: true, completion: nil)
    }
    
    
    @IBAction func invitadoButtonPressed(_ sender: Any) {
    }
    
}

